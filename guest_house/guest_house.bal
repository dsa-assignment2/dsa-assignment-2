import ballerinax/kafka;
import ballerina/log;

kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "TheGroceryCompany",
    topics: ["TheGroceryCompany-GuestHouse"],
    offsetReset: "earliest",
    pollingInterval: 1
};

public type Order record {
    int id;
    string customerNumber;
    Request[] request;
    string address;
};

public type Request record {
    int productId;
    int quantity;
};

public type OrderConsumerRecord record {|
    *kafka:AnydataConsumerRecord;
    Order value;
|};

service on new kafka:Listener(kafka:DEFAULT_URL, consumerConfigs) {
    remote function onConsumerRecord(OrderConsumerRecord[] records) returns error? {
        records.forEach(function(OrderConsumerRecord orderRecord) {
            log:printInfo("The valid Order Has Been succesfully Recieved: " + orderRecord.value.toString());
        });

    }
}

