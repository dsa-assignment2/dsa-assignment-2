
import ballerina/http;
import ballerinax/mongodb;
import ballerina/io;
import ballerinax/kafka;

public type Thegrocerycompany record {
    int id;
    string name;
    float price;
    GuestHouse[] store;
    boolean isAvailable;
};

public type GuestHouse record {
    string name;
    int quantity;
};

public type Order record {
    int id;
    string customerNumber;
    Request[] request;
    string address;
};

public type Request record {
    int productId;
    int quantity;
};

mongodb:ConnectionConfig mongoConfig = {
    host: "localhost",
    port: 27017,
    options: {sslEnabled: false, serverSelectionTimeout: 5000}
};

mongodb:Client mongoClient = checkpanic new (mongoConfig, "TheGroceryCompany_store");

public type OrderProducerRecord record {|
    *kafka:AnydataProducerRecord;
    Order value;
    int key;
|};

kafka:Producer orderProducer = check new (kafka:DEFAULT_URL);

service /TheGroceryCompany_store on new http:Listener(5000) {

    resource function get receiveTheGroceryCompany() returns Thegrocerycompany[]|error {

        stream<Thegrocerycompany, error?> thegrocerycompany = check mongoClient->find("TheGroceryCompany", filter = {isAvailable: true});

        io:print(thegrocerycompany);
        Thegrocerycompany[] data = [];
        check thegrocerycompany.forEach(function(Thegrocerycompany Thegrocerycompany) {
            data.push(Thegrocerycompany);
        });

        return data;
    }

    resource function post enclosureOrders(http:Caller caller, http:Request request) returns error? {
        http:Response response = new;
        json|error reqPayload = request.getJsonPayload();

        if (reqPayload is error) {
            response.statusCode = 400;
            response.setJsonPayload({"Message": "Invalid payload - Not a valid JSON payload"});
            var result = caller->respond(response);
            io:print(result);
        } else {

            //get full order
            Order reqOrder = check reqPayload.cloneWithType();

            int[] availableIds = reqOrder.request.'map(x => x.productId);

            //get all groceries
            stream<Thegrocerycompany, error?> groceries = check mongoClient->find("TheGroceryCompany", filter = {isAvailable: true});

            Thegrocerycompany[] result = [];

            check groceries.forEach(function(Thegrocerycompany Thegrocerycompany) {
                foreach int item in availableIds {
                    if (Thegrocerycompany.id == item) {
                        result.push(Thegrocerycompany);
                    }
                }
            });

            string Availability = "";

            foreach Thegrocerycompany item in result {
                int sum = 0;
                foreach GuestHouse value in item.store {
                    sum += value.quantity;
                }

                foreach Request actual in reqOrder.request {
                    if (actual.productId == item.id && actual.quantity > sum) {
                        Availability = "Error";
                    }
                }
            }

            if (Availability == "Error") {
                response.statusCode = 400;
                response.setJsonPayload({"Message": "Available quantity of an item is less than what is ordered. adjust the quantity, cancel the item or cancel the order altogether"});
                var responseResu = caller->respond(response);
                io:print(responseResu);
            } else {

                map<json> saveRequest = <map<json>>reqOrder.toJson();

                OrderProducerRecord producerRecord = {
                    key: 1,
                    topic: "TheGroceryCompany-GuestHouse",
                    value: reqOrder
                };

                // Sends the message to the Kafka topic.
                check orderProducer->send(producerRecord);

                check mongoClient->insert(saveRequest, "Order");
                mongoClient->close();

                response.setJsonPayload({"Status": "Success"});
                var responseResult = caller->respond(response);
                io:print(responseResult);
            }

        }

    }
}

